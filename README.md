build arm64 image from https://github.com/jetstack/kube-oidc-proxy.git

# git
```
docker run -it -v $PWD:/workdir alpine/git -C /workdir clone https://github.com/jetstack/kube-oidc-proxy.git
```
# golang
```
docker run -v $PWD/kube-oidc-proxy:/workdir -it golang bash
```
```
cd /workdir
make build
#GOARCH=$(ARCH) GOOS=linux CGO_ENABLED=0 go build -ldflags '-w $(shell hack/version-ldflags.sh)' -o ./bin/kube-oidc-proxy-linux  ./cmd/.
cp ./bin/kube-oidc-proxy ./bin/kube-oidc-proxy-linux
```
# kaniko
```
cd /workdir
Dockerfile
```

# TL;DR;

```
antoinenguyen31/kube-oidc-proxy:arm64
```
